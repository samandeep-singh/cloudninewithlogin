package sheridan;

/*
 * 
 *@author Samandeep Singh
 */

public class LoginValidator {

	public static boolean isValidLoginName( String loginName ) {
		
		return loginName.length() >= 6 && !Character.isDigit(loginName.charAt(0)) &&  loginName.matches("^[A-Za-z0-9]+$");
		
	}
}
