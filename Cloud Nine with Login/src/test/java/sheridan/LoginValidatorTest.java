package sheridan;

import static org.junit.Assert.*;

import org.junit.Test;

/**
 * 
 * @author Samandeep Singh
 *
 */

public class LoginValidatorTest {

	@Test
	public void testIsValidLoginNameFormatRegular() {
		boolean isValidFormat = LoginValidator.isValidLoginName("arthurmorgan");
		assertTrue("Invalid Login Name format!", isValidFormat);
	}
	
	@Test
	public void testIsValidLoginNameFormatException() {
		boolean isValidFormat = LoginValidator.isValidLoginName("1jh#m");
		assertFalse("Invalid Login Name format!", isValidFormat);
	}
	
	@Test
	public void testIsValidLoginNameFormatBoundaryOut() {
		boolean isValidFormat = LoginValidator.isValidLoginName("john*marston");
		assertFalse("Invalid Login Name format!", isValidFormat);
	}
	
	@Test
	public void testIsValidLoginNameFormatBoundaryIn() {
		boolean isValidFormat = LoginValidator.isValidLoginName("abigailroberts5");
		assertTrue("Invalid Login Name format!", isValidFormat);
	}
	
	@Test
	public void testHasValidLengthRegular() {
		boolean hasValidLength = LoginValidator.isValidLoginName("michaelkahnwald");
		assertTrue("Invalid Login Name length!", hasValidLength);
	}
	
	@Test
	public void testHasValidLengthException() {
		boolean hasValidLength = LoginValidator.isValidLoginName("sam");
		assertFalse("Invalid Login Name length!", hasValidLength);
	}
	
	@Test
	public void testHasValidLengthBoundaryOut() {
		boolean hasValidLength = LoginValidator.isValidLoginName("james");
		assertFalse("Invalid Login Name length!", hasValidLength);
	}
	
	@Test
	public void testHasValidLengthBoundaryIn() {
		boolean hasValidLength = LoginValidator.isValidLoginName("martha");
		assertTrue("Invalid Login Name length!", hasValidLength);
	}

}
